import numpy as np
from skimage.measure import find_contours


def compute_efsd(mask, N):
    """Compute the elliptical Fourier shape descriptors for the N first
    harmonics of the contour of a given mask.

    Args:
        mask (ndarray): binary mask to be processed.
        N (int): Number of harmonics to consider for the computation
            of descriptors

    Returns:
        (ndarray): Array of Elliptical Fourier shape descriptors with
            shape (N+1,4)

    """
    contour = find_contours(mask, level=0)[0]
    dxy = np.diff(contour, axis=0)
    dt = np.sqrt((dxy ** 2).sum(axis=1))
    t = np.concatenate([([0., ]), np.cumsum(dt)])
    T = t[-1]
    phi = (2 * np.pi * t) / T

    efsd = np.zeros((N+1, 4))
    xsi = np.cumsum(dxy[:, 0]) - (dxy[:, 0] / dt) * t[1:]
    delta = np.cumsum(dxy[:, 1]) - (dxy[:, 1] / dt) * t[1:]
    a_0 = (1 / T) * np.sum(((dxy[:, 0] / (2 * dt)) *
                            np.diff(t ** 2)) + xsi * dt)
    c_0 = (1 / T) * np.sum(((dxy[:, 1] / (2 * dt)) *
                            np.diff(t ** 2)) + delta * dt)
    efsd[0, 0] = contour[0, 0] + a_0
    efsd[0, 2] = contour[0, 1] + c_0
    for n in range(1, N+1):
        const = T / (2 * n * n * np.pi * np.pi)
        phi_n = phi * n
        d_cos_phi_n = np.cos(phi_n[1:]) - np.cos(phi_n[:-1])
        d_sin_phi_n = np.sin(phi_n[1:]) - np.sin(phi_n[:-1])
        a_n = const * np.sum((dxy[:, 0] / dt) * d_cos_phi_n)
        b_n = const * np.sum((dxy[:, 0] / dt) * d_sin_phi_n)
        c_n = const * np.sum((dxy[:, 1] / dt) * d_cos_phi_n)
        d_n = const * np.sum((dxy[:, 1] / dt) * d_sin_phi_n)
        efsd[n, :] = a_n, b_n, c_n, d_n

    return np.concatenate([[efsd[0, 0], efsd[0, 2]], efsd[1:, :].ravel()])


def get_cell_efsd(ms, harmonic_count):
    bg_val, cyto_val, nucleus_val = np.unique(ms)
    cell_mask = ms == cyto_val
    nucleus_mask = ms == nucleus_val

    cell_efsd = compute_efsd(cell_mask, harmonic_count)
    nucleus_efsd = compute_efsd(nucleus_mask, harmonic_count)
    return np.concatenate([cell_efsd, nucleus_efsd])


def reconstruct_contour(efsd, K):
    """Reconstruct the contour from given elliptical Fourier shape
    descriptors

    Args:
        efsd (ndarray): Elliptical Fourier shape descriptors of shape
            (N+1, 4) where N is the harmonic count.
        K (int): Number of discretization points

    Returns:
        (ndarray): Reconstructed contour of shape (K,2)

    """
    t = np.linspace(0, 1.0, K)
    x = np.ones((K,)) * efsd[0]
    y = np.ones((K,)) * efsd[1]

    for n in range(2, efsd.shape[0], 4):
        a = 2*(n//4 + 1) * np.pi * t
        c, s = np.cos(a), np.sin(a)
        x += ((efsd[n] * c) + (efsd[n+1] * s))
        y += ((efsd[n+2] * c) + (efsd[n+3] * s))

    return np.array([x, y]).T
